package edu.systemia.common.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends HandleException {
	public NotFoundException(String message) {
		super(message, HttpStatus.NOT_FOUND);
	}
}
