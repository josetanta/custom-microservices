package edu.systemia.common.exceptions;

import org.springframework.http.HttpStatus;

public class HandleException extends RuntimeException {
	private final HttpStatus statusCode;

	public HandleException(String message, HttpStatus statusCode) {
		super(message);
		this.statusCode = statusCode;
	}

	public Integer getStatusCode() {
		return statusCode.value();
	}
}
