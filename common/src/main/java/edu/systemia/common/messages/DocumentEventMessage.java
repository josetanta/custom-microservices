package edu.systemia.common.messages;

import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DocumentEventMessage extends BaseEvent {

	private Long id;

	private String nameDoc;

	private Long employeeId;
}
