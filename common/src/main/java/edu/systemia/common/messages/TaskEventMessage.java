package edu.systemia.common.messages;

import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TaskEventMessage extends BaseEvent {
	private long id;
	private String title;
	private String slug;
	private boolean completed;
	private long ownerId;
}
