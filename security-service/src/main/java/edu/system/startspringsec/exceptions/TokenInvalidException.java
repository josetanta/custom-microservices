package edu.system.startspringsec.exceptions;

public class TokenInvalidException extends NullPointerException {
	public TokenInvalidException(String message) {
		super(message);
	}
}
