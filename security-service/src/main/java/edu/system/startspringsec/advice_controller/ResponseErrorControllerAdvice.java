package edu.system.startspringsec.advice_controller;

import edu.system.startspringsec.exceptions.TokenInvalidException;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.MalformedJwtException;
import jakarta.validation.ValidationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ResponseErrorControllerAdvice {

	private final Map<String, Object> errors = new HashMap<>();

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({MethodArgumentNotValidException.class})
	public Map<String, Object> handleValidRequestBodyException(MethodArgumentNotValidException ex) {
		ex.getBindingResult().getAllErrors().forEach(err -> {
			var fieldName = ((FieldError) err).getField();
			var errorMessage = err.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});
		return errors;
	}

	@ExceptionHandler({
		IOException.class,
		JwtException.class,
		AuthenticationException.class,
		AccessDeniedException.class,
		ValidationException.class,
		DataIntegrityViolationException.class,
		NullPointerException.class,
		UsernameNotFoundException.class,
		MalformedJwtException.class
	})
	public ProblemDetail handleSecExceptions(Exception ex) {
		ProblemDetail problemDetail = null;

		if (ex instanceof BadCredentialsException badEx) {
			problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.UNAUTHORIZED, badEx.getLocalizedMessage());
			problemDetail.setProperty("access_denied_reason", "bad_credentials");
		}

		if (ex instanceof AccessDeniedException accEx) {
			problemDetail = ProblemDetail
				.forStatusAndDetail(HttpStatus.UNAUTHORIZED, accEx.getMessage());
			problemDetail.setProperty("access_denied_reason", "not_authorized");
		}

		if (ex instanceof ExpiredJwtException jwtException) {
			problemDetail = ProblemDetail
				.forStatusAndDetail(HttpStatus.BAD_REQUEST, jwtException.getMessage());
			problemDetail.setTitle("TokenInvalid");
			problemDetail.setProperty("access_denied_reason", "token_is_invalid");
		}

		if (ex instanceof TokenInvalidException token) {
			problemDetail = ProblemDetail
				.forStatusAndDetail(HttpStatus.BAD_REQUEST, token.getMessage());
			problemDetail.setTitle("TokenInvalid");
			problemDetail.setProperty("access_denied_reason", "malformed_token");
		}

		if (ex instanceof DataIntegrityViolationException consEx) {
			problemDetail = ProblemDetail
				.forStatusAndDetail(HttpStatus.BAD_REQUEST, consEx.getMessage());
			problemDetail.setTitle("Constraint Violation");
			problemDetail.setProperty("access_denied_reason", "constraint_violation");
		}

		if (ex instanceof UsernameNotFoundException userEx) {
			problemDetail = ProblemDetail
				.forStatusAndDetail(HttpStatus.BAD_REQUEST, userEx.getMessage());
			problemDetail.setTitle("User doesn't exist");
		}

		if (ex instanceof MalformedJwtException malEx) {
			problemDetail = ProblemDetail
				.forStatusAndDetail(HttpStatus.NOT_ACCEPTABLE, malEx.getMessage());
		}

		return problemDetail;
	}
}