package edu.system.startspringsec.usecases;


import edu.system.startspringsec.infrastructure.outputs.ports.TokenRepository;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LogoutUseCase implements LogoutHandler {

	private final TokenRepository tokenRepository;

	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		final var authHeader = request.getHeader("Authorization");

		if (authHeader != null && !authHeader.startsWith("Bearer ")) {
			return;
		}
		assert authHeader != null;
		final var jwt = authHeader.substring(7);
		var storedToken = tokenRepository.findByHashToken(jwt).orElse(null);

		if (storedToken != null) {
			storedToken.setExpired(true);
			storedToken.setRevoked(true);
			tokenRepository.save(storedToken);
		}
		response.setStatus(HttpStatus.NO_CONTENT.value());
	}
}
