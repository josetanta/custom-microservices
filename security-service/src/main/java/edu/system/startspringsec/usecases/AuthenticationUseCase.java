package edu.system.startspringsec.usecases;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.system.startspringsec.domain.roles.Role;
import edu.system.startspringsec.domain.user.Token;
import edu.system.startspringsec.domain.user.TokenType;
import edu.system.startspringsec.domain.user.User;
import edu.system.startspringsec.infrastructure.dto.AuthenticationRequest;
import edu.system.startspringsec.infrastructure.dto.AuthenticationResponse;
import edu.system.startspringsec.infrastructure.dto.RegisterRequest;
import edu.system.startspringsec.infrastructure.outputs.ports.TokenRepository;
import edu.system.startspringsec.infrastructure.outputs.ports.UserRepository;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

@Service
@RequiredArgsConstructor
@Transactional
public class AuthenticationUseCase {

	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;
	private final JwtUseCase jwtUseCase;
	private final AuthenticationManager authenticationManager;
	private final TokenRepository tokenRepository;

	public void register(RegisterRequest registerRequest) {
		var user = registerRequest.toUser();
		// encoded password
		user.setRole(Role.USER);
		user.setPassword(passwordEncoder.encode(user.getPassword()));

		// Save user
		userRepository.save(user);
	}


	public AuthenticationResponse authenticate(AuthenticationRequest request) {
		authenticationManager.authenticate(
			new UsernamePasswordAuthenticationToken(
				request.email(),
				request.password()
			)
		);
		var user = userRepository
			.findByEmail(request.email())
			.orElseThrow();

		var token = jwtUseCase.generateToken(user);
		var refreshToken = jwtUseCase.generateRefreshToken(user);
		revokeAllUserTokens(user);
		saveUserAndToken(user, token);

		return new AuthenticationResponse(token, refreshToken);
	}

	private void saveUserAndToken(User savedUser, String jwtToken) {
		var token = Token.builder()
			.user(savedUser)
			.hashToken(jwtToken)
			.tokenType(TokenType.BEARER)
			.revoked(false)
			.expired(false)
			.build();
		tokenRepository.save(token);
	}

	private void revokeAllUserTokens(User user) {
		var validUserTokens = tokenRepository.findAllByValidTokensByUser(user.getId());
		if (validUserTokens.isEmpty()) return;

		validUserTokens.forEach(token -> {
			token.setExpired(true);
			token.setRevoked(true);
		});

		tokenRepository.saveAll(validUserTokens);
	}

	public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
		final var refreshToken = request.getHeader("Authorization").substring(7);
		final var username = jwtUseCase.extractUsername(refreshToken);

		var user = userRepository.findByEmail(username).orElseThrow();

		if (jwtUseCase.isTokenValid(refreshToken, user)) {
			var accessToken = jwtUseCase.generateToken(user);
			saveUserAndToken(user, accessToken);
			var authResponse = new AuthenticationResponse(accessToken, refreshToken);
			response.setContentType(MediaType.APPLICATION_JSON_VALUE);
			new ObjectMapper().writeValue(response.getOutputStream(), authResponse);
		}
	}
}
