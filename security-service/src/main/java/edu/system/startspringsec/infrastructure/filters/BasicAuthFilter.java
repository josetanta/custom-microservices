package edu.system.startspringsec.infrastructure.filters;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Base64;
import java.util.Objects;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Slf4j
@RequiredArgsConstructor
public class BasicAuthFilter extends OncePerRequestFilter {

	private final UserDetailsService userDetailsService;

	@Override
	protected void doFilterInternal(
		@NonNull HttpServletRequest request,
		@NonNull HttpServletResponse response,
		@NonNull FilterChain filterChain
	) throws ServletException, IOException {
		final String headerAuth = request.getHeader(AUTHORIZATION);
		if (Objects.isNull(headerAuth) || !headerAuth.startsWith("Basic ")) {
			filterChain.doFilter(request, response);
			return;
		}

		String[] credentials = extractAndDecodeHeader(headerAuth);
		if (Objects.isNull(credentials)) {
			response.sendError(HttpStatus.BAD_REQUEST.value(), "Invalid credentials");
			return;
		}

		if (SecurityContextHolder.getContext().getAuthentication() == null) {
			final var userDetails = userDetailsService.loadUserByUsername(credentials[0]);
			final var authToken = new UsernamePasswordAuthenticationToken(userDetails, credentials[1]);
			authToken.setDetails(
				new WebAuthenticationDetailsSource().buildDetails(request)
			);
			SecurityContextHolder.getContext().setAuthentication(authToken);
			log.info("Perform authentication user {}", userDetails);
		}
		filterChain.doFilter(request, response);
	}

	private String[] extractAndDecodeHeader(String headerAuth) {
		byte[] decoded;
		try {
			decoded = Base64.getDecoder().decode(headerAuth.substring(6));
			String token = new String(decoded);
			return token.split(":");
		} catch (Exception ex) {
			return null;
		}
	}

	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) {
		return Boolean.TRUE.equals(
			request.getServletPath().contains("/api/auth")
				|| request.getServletPath().contains("/swagger")
				|| request.getServletPath().contains("/api-docs")
				|| request.getServletPath().contains("/configuration")
		);
	}
}
