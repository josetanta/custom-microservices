package edu.system.startspringsec.infrastructure.inputs.adapters;

import edu.system.startspringsec.infrastructure.dto.AuthenticationRequest;
import edu.system.startspringsec.infrastructure.dto.AuthenticationResponse;
import edu.system.startspringsec.infrastructure.dto.RegisterRequest;
import edu.system.startspringsec.usecases.AuthenticationUseCase;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class AuthenticationAPI {

	private final AuthenticationUseCase authenticationUseCase;

	@PostMapping(value = "/auth/register")
	public ResponseEntity<Object> register(
		@Valid @RequestBody RegisterRequest registerRequest
	) {
		authenticationUseCase.register(registerRequest);
		var resp = new HashMap<String, String>();
		resp.put("message", "Register successfully");

		return ResponseEntity.ok(resp);
	}

	@PostMapping(value = "/auth/sign")
	public ResponseEntity<AuthenticationResponse> signIn(
		@Valid @RequestBody AuthenticationRequest request
	) {
		return ResponseEntity.ok(authenticationUseCase.authenticate(request));
	}

	@PostMapping("/refresh-token")
	public void requestRefreshToken(
		HttpServletRequest request,
		HttpServletResponse response
	) throws IOException {
		authenticationUseCase.refreshToken(request, response);
	}
}
