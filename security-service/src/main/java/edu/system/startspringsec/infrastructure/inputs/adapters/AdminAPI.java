package edu.system.startspringsec.infrastructure.inputs.adapters;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/admin")
@Tag(name = "Administer")
public class AdminAPI {

	@Operation(
		description = "Get endpoint for admin.",
		summary = "This endpoint return a list of devices available.",
		responses = {
			@ApiResponse(
				description = "Successfully",
				responseCode = "200"
			),
			@ApiResponse(
				description = "Forbidden",
				responseCode = "403"
			)
		}
	)
	@GetMapping
	public String getResource() {
		return "GET:: Admin Controller";
	}

	@PostMapping
	public String postResource() {
		return "POST:: Admin Controller";
	}

	@PutMapping
	public String putResource() {
		return "PUT:: Admin Controller";
	}

	@PatchMapping
	public String patchResource() {
		return "PATCH:: Admin Controller";
	}

	@DeleteMapping
	public String deleteResource() {
		return "DELETE:: Admin Controller";
	}
}
