package edu.system.startspringsec.infrastructure.dto;

import edu.system.startspringsec.domain.roles.Role;
import edu.system.startspringsec.domain.user.User;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.NonNull;

public record RegisterRequest(

	@Email
	String email,

	@NotBlank
	String firstname,

	@NotBlank
	String lastname,

	@NotNull
	@Size(
		min = 5,
		max = 20,
		message = "Your password must be between 5 and 20 characters"
	)
	String password,

	Role role
) {

	@NonNull
	public User toUser() {

		return User.builder()
			.firstname(firstname)
			.lastname(lastname)
			.email(email)
			.password(password)
			.role(role)
			.build();
	}
}
