package edu.system.startspringsec.infrastructure.inputs.adapters;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/demo")
public class DemoAPI {

	@GetMapping
	public ResponseEntity<String> say() {
		return ResponseEntity.ok("Successfully");
	}
}
