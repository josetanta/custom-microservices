package edu.system.startspringsec.infrastructure.filters;


import edu.system.startspringsec.exceptions.TokenInvalidException;
import edu.system.startspringsec.infrastructure.outputs.ports.TokenRepository;
import edu.system.startspringsec.usecases.JwtUseCase;
import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;


@Slf4j
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {
	private static final String BEARER_SCHEMA = "Bearer ";
	private final JwtUseCase jwtUseCase;
	private final UserDetailsService userDetailsService;
	private final TokenRepository tokenRepository;
	private final HandlerExceptionResolver exceptionResolver;

	@Override
	protected void doFilterInternal(
		@NonNull HttpServletRequest request,
		@NonNull HttpServletResponse response,
		@NonNull FilterChain filterChain
	) {
		try {
			final var authHeader = request.getHeader(AUTHORIZATION);
			if (authHeader == null || !authHeader.startsWith(BEARER_SCHEMA)) {
				throw new TokenInvalidException("Token has malformed structure");
			}

			final var jwtToken = authHeader.substring(7);
			final var userEmail = jwtUseCase.extractUsername(jwtToken);

			log.debug("VERIFY: {}", userEmail);

			if (userEmail != null && SecurityContextHolder.getContext().getAuthentication() == null) {
				var userDetails = this.userDetailsService.loadUserByUsername(userEmail);

				var canTokenAccess = tokenRepository
					.findByHashToken(jwtToken)
					.map(t -> !t.isExpired() && !t.isRevoked())
					.orElse(false);

				if (jwtUseCase.isTokenValid(jwtToken, userDetails) && canTokenAccess) {
					var authToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
					authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
					SecurityContextHolder.getContext().setAuthentication(authToken);
				}
			}
			filterChain.doFilter(request, response);
		} catch (Exception ex) {
			exceptionResolver.resolveException(request, response, null, ex);
		}
	}

	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) {
		return Boolean.TRUE.equals(
			request.getServletPath().contains("/api/auth")
				|| request.getServletPath().contains("/swagger")
				|| request.getServletPath().contains("/api-docs")
				|| request.getServletPath().contains("/configuration")
		);
	}
}
