package edu.system.startspringsec.infrastructure.dto;

public record AuthenticationResponse(
	String accessToken,
	String refreshToken
) {
}
