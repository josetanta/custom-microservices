package edu.system.startspringsec.infrastructure.inputs.adapters;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/management")
@Tag(name = "Managment")
public class ManagementAPI {

	@Operation(
		description = "Return a device for a user",
		responses = {
			@ApiResponse(
				description = "Success",
				responseCode = "200"
			),
			@ApiResponse(
				description = "Forbidden",
				responseCode = "403"
			)
		}
	)
	@GetMapping
	public String getResource() {
		return "GET:: Management Controller";
	}

	@PostMapping
	public String postResource() {
		return "POST:: Management Controller";
	}

	@PutMapping
	public String putResource() {
		return "PUT:: Management Controller";
	}

	@PatchMapping
	public String patchResource() {
		return "PATCH:: Management Controller";
	}

	@DeleteMapping
	public String deleteResource() {
		return "DELETE:: Management Controller";
	}
}
