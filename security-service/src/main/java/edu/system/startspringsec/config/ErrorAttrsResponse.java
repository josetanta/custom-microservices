package edu.system.startspringsec.config;

import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

@Component
public class ErrorAttrsResponse extends DefaultErrorAttributes {

	@Override
	public Map<String, Object> getErrorAttributes(WebRequest webRequest, ErrorAttributeOptions options) {
		var errorAttrs = super.getErrorAttributes(webRequest, options);
		errorAttrs.put("custom-message", "error from attr");

		return errorAttrs;
	}
}
