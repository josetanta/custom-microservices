package edu.system.startspringsec.config;


import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition(
	info = @Info(
		contact = @Contact(
			name = "josetanta",
			email = "jose.tanta.27@unsch.edu.pe",
			url = "https://josetanta.com"
		),
		description = "OpenAPI documentation",
		title = "OpenAPI specification - Jose Tanta",
		version = "1.0.0",
		license = @License(
			name = "License Apache",
			url = "https://apache.com"
		),
		termsOfService = "Terms of service"
	),
	servers = {
		// Urls about local and production
		@Server(
			url = "http://localhost:8080",
			description = "Local development"
		)
	},
	security = {
		@SecurityRequirement(
			name = "bearerAuth"
		)
	}
)
@SecurityScheme(
	name = "bearerAuth",
	description = "JWT auth description",
	scheme = "bearer",
	type = SecuritySchemeType.HTTP,
	bearerFormat = "JWT",
	in = SecuritySchemeIn.HEADER
)
public class OpenAPIConfig {
}
