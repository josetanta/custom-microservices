package edu.system.startspringsec.config;

import edu.system.startspringsec.domain.roles.Permissions;
import edu.system.startspringsec.domain.roles.Role;
import edu.system.startspringsec.infrastructure.filters.BasicAuthFilter;
import edu.system.startspringsec.infrastructure.filters.JwtAuthenticationFilter;
import edu.system.startspringsec.usecases.LogoutUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Order(1)
@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig {

	public static final String[] PUBLIC_URL = {
		"/api/auth/**",
		"/v2/api-docs",
		"/v3/api-docs",
		"/v3/api-docs/**",
		"/swagger-resources",
		"/swagger-resources/**",
		"/configuration/ui",
		"/configuration/security",
		"/swagger-ui/**",
		"/webjars/**",
		"/swagger-ui.html"
	};

	private final JwtAuthenticationFilter jwtAuthFilter;
	private final BasicAuthFilter basicAuthFilter;
	private final AuthenticationProvider authenticationProvider;
	private final LogoutUseCase logoutUseCase;

	@Bean
	@ConditionalOnProperty(name = "application.security.type-auth", havingValue = "AUTH02")
	public SecurityFilterChain securityFilterChainJwt(HttpSecurity http) throws Exception {
		http
			.csrf(AbstractHttpConfigurer::disable)
			.authorizeHttpRequests(authorize ->
				authorize
					.requestMatchers(PUBLIC_URL)
					.permitAll()
					.requestMatchers("/api/management/**").hasAnyRole(Role.ADMIN.name(), Role.MANAGER.name())
					.requestMatchers(HttpMethod.GET, "/api/management/**").hasAnyAuthority(Permissions.MANAGER_READ.name(), Permissions.ADMIN_READ.name())
					.requestMatchers(HttpMethod.POST, "/api/management/**").hasAnyAuthority(Permissions.MANAGER_CREATE.name(), Permissions.ADMIN_CREATE.name())
					.requestMatchers(HttpMethod.PUT, "/api/management/**").hasAnyAuthority(Permissions.MANAGER_UPDATE.name(), Permissions.ADMIN_UPDATE.name())
					.requestMatchers(HttpMethod.DELETE, "/api/management/**").hasAnyAuthority(Permissions.MANAGER_DELETE.name(), Permissions.ADMIN_DELETE.name())
					.requestMatchers(HttpMethod.PATCH, "/api/management/**").hasAnyAuthority(Permissions.MANAGER_UPDATE_PARTIAL.name(), Permissions.ADMIN_UPDATE_PARTIAL.name())

					.requestMatchers("/api/admin/**").hasRole(Role.ADMIN.name())
					.requestMatchers(HttpMethod.GET, "/api/admin/**").hasAuthority(Permissions.ADMIN_READ.name())
					.requestMatchers(HttpMethod.POST, "/api/admin/**").hasAuthority(Permissions.ADMIN_CREATE.name())
					.requestMatchers(HttpMethod.PUT, "/api/admin/**").hasAuthority(Permissions.ADMIN_UPDATE.name())
					.requestMatchers(HttpMethod.DELETE, "/api/admin/**").hasAuthority(Permissions.ADMIN_DELETE.name())
					.requestMatchers(HttpMethod.PATCH, "/api/admin/**").hasAuthority(Permissions.ADMIN_UPDATE_PARTIAL.name())
					.anyRequest()
					.authenticated()
			)
			.sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
			.authenticationProvider(authenticationProvider)
			.addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class)
			.logout(logout -> logout
				.logoutUrl("/api/auth/logout")
				.addLogoutHandler(logoutUseCase)
				.logoutSuccessHandler((request, response, auth) -> SecurityContextHolder.clearContext())
			);
		return http.build();
	}

	@Bean
	@ConditionalOnProperty(name = "application.security.type-auth", havingValue = "BASIC", matchIfMissing = true)
	public SecurityFilterChain securityFilterChainBasic(HttpSecurity http) throws Exception {
		http
			.csrf(AbstractHttpConfigurer::disable)
			.authorizeHttpRequests(authorize ->
				authorize
					.requestMatchers(PUBLIC_URL)
					.permitAll()
					.requestMatchers("/api/management/**").hasAnyRole(Role.ADMIN.name(), Role.MANAGER.name())
					.requestMatchers(HttpMethod.GET, "/api/management/**").hasAnyAuthority(Permissions.MANAGER_READ.name(), Permissions.ADMIN_READ.name())
					.requestMatchers(HttpMethod.POST, "/api/management/**").hasAnyAuthority(Permissions.MANAGER_CREATE.name(), Permissions.ADMIN_CREATE.name())
					.requestMatchers(HttpMethod.PUT, "/api/management/**").hasAnyAuthority(Permissions.MANAGER_UPDATE.name(), Permissions.ADMIN_UPDATE.name())
					.requestMatchers(HttpMethod.DELETE, "/api/management/**").hasAnyAuthority(Permissions.MANAGER_DELETE.name(), Permissions.ADMIN_DELETE.name())
					.requestMatchers(HttpMethod.PATCH, "/api/management/**").hasAnyAuthority(Permissions.MANAGER_UPDATE_PARTIAL.name(), Permissions.ADMIN_UPDATE_PARTIAL.name())

					.requestMatchers("/api/admin/**").hasRole(Role.ADMIN.name())
					.requestMatchers(HttpMethod.GET, "/api/admin/**").hasAuthority(Permissions.ADMIN_READ.name())
					.requestMatchers(HttpMethod.POST, "/api/admin/**").hasAuthority(Permissions.ADMIN_CREATE.name())
					.requestMatchers(HttpMethod.PUT, "/api/admin/**").hasAuthority(Permissions.ADMIN_UPDATE.name())
					.requestMatchers(HttpMethod.DELETE, "/api/admin/**").hasAuthority(Permissions.ADMIN_DELETE.name())
					.requestMatchers(HttpMethod.PATCH, "/api/admin/**").hasAuthority(Permissions.ADMIN_UPDATE_PARTIAL.name())
					.anyRequest()
					.authenticated()
			)
			.sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
			.authenticationProvider(authenticationProvider)
			.addFilterBefore(basicAuthFilter, UsernamePasswordAuthenticationFilter.class);
		return http.build();
	}
}
