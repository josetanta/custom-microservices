package edu.system.startspringsec.config;


import edu.system.startspringsec.infrastructure.filters.BasicAuthFilter;
import edu.system.startspringsec.infrastructure.filters.JwtAuthenticationFilter;
import edu.system.startspringsec.infrastructure.outputs.ports.TokenRepository;
import edu.system.startspringsec.infrastructure.outputs.ports.UserRepository;
import edu.system.startspringsec.usecases.JwtUseCase;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerExceptionResolver;

import static org.springframework.http.MediaType.*;

@Configuration
public class BeansConfig {

	private final UserRepository userRepository;
	private final TokenRepository tokenRepository;
	private final JwtUseCase jwtUseCase;
	private final HandlerExceptionResolver exceptionResolver;

	public BeansConfig(
		UserRepository userRepository,
		TokenRepository tokenRepository,
		JwtUseCase jwtUseCase,

		@Qualifier("handlerExceptionResolver")
		HandlerExceptionResolver exceptionResolver
	) {
		this.userRepository = userRepository;
		this.tokenRepository = tokenRepository;
		this.jwtUseCase = jwtUseCase;
		this.exceptionResolver = exceptionResolver;
	}

	@Bean
	public UserDetailsService userDetailsService() {
		return username -> userRepository.findByEmail(username)
			.orElseThrow(() -> new UsernameNotFoundException("User not found"));
	}

	@Bean
	public AuthenticationProvider authenticationProvider() {
		var authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService());
		authProvider.setPasswordEncoder(passwordEncoder());
		return authProvider;
	}

	@Bean
	public AuthenticationManager authenticationManager(AuthenticationConfiguration configuration) throws Exception {
		return configuration.getAuthenticationManager();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public JwtAuthenticationFilter jwtAuthenticationFilter() {
		return new JwtAuthenticationFilter(jwtUseCase, userDetailsService(), tokenRepository, exceptionResolver);
	}

	@Bean
	public BasicAuthFilter basicAuthFilter() {
		return new BasicAuthFilter(userDetailsService());
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplateBuilder()
			.defaultHeader("Accept",
				APPLICATION_JSON_VALUE,
				APPLICATION_FORM_URLENCODED_VALUE,
				MULTIPART_FORM_DATA_VALUE
			)
			.build();
	}
}
