package edu.system.startspringsec.domain.roles;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@RequiredArgsConstructor
public enum Role {

	USER(Collections.emptySet()),

	ADMIN(
		Set.of(
			Permissions.ADMIN_READ,
			Permissions.ADMIN_DELETE,
			Permissions.ADMIN_UPDATE,
			Permissions.ADMIN_UPDATE_PARTIAL,
			Permissions.ADMIN_CREATE,

			Permissions.MANAGER_READ,
			Permissions.MANAGER_DELETE,
			Permissions.MANAGER_UPDATE,
			Permissions.MANAGER_UPDATE_PARTIAL,
			Permissions.MANAGER_CREATE
		)
	),

	MANAGER(
		Set.of(
			Permissions.MANAGER_READ,
			Permissions.MANAGER_DELETE,
			Permissions.MANAGER_UPDATE,
			Permissions.MANAGER_UPDATE_PARTIAL,
			Permissions.MANAGER_CREATE
		)
	);

	private final Set<Permissions> permissions;

	public List<SimpleGrantedAuthority> getAuthorities() {
		var authorities = getPermissions().stream()
			.map(permission -> new SimpleGrantedAuthority(permission.name()))
			.collect(Collectors.toList());
		authorities.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
		return authorities;
	}
}
