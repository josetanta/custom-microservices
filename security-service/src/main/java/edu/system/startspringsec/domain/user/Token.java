package edu.system.startspringsec.domain.user;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tokens")
public class Token {

	@Id
	@GeneratedValue
	private Long id;
	private String hashToken;

	@Enumerated(EnumType.STRING)
	private TokenType tokenType;
	private boolean expired;
	private boolean revoked;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
}
