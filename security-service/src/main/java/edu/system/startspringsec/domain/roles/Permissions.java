package edu.system.startspringsec.domain.roles;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Permissions {

	ADMIN_READ("admin:read"),
	ADMIN_UPDATE("admin:update"),
	ADMIN_CREATE("admin:create"),
	ADMIN_DELETE("admin:delete"),
	ADMIN_UPDATE_PARTIAL("admin:update_partial"),

	MANAGER_READ("manager:read"),
	MANAGER_UPDATE("manager:update"),
	MANAGER_CREATE("manager:create"),
	MANAGER_DELETE("manager:delete"),
	MANAGER_UPDATE_PARTIAL("manager:update_partial");

	private final String permission;
}
