package edu.system.startspringsec;

import edu.system.startspringsec.domain.roles.Role;
import edu.system.startspringsec.infrastructure.dto.RegisterRequest;
import edu.system.startspringsec.usecases.AuthenticationUseCase;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SecurityServiceApplication {

	public static void main(String... args) {
		SpringApplication.run(SecurityServiceApplication.class, args);
	}

	public CommandLineRunner commandLineRunner(AuthenticationUseCase authenticationUseCase) {

		return args -> {
			var admin = new RegisterRequest("gabriel@mail.com", "jose", "tanta", "password", Role.ADMIN);
			authenticationUseCase.register(admin);

			var manager = new RegisterRequest("jose@mail.com", "gabriel", "calderon", "password", Role.MANAGER);
			authenticationUseCase.register(manager);
		};
	}
}
