### Ports

- registry-service: 8761
- gateway-service: 8080 (Proxy Server)
- documents-service: 8081
- tasks-service: 8082

#### Graphics
<img src="architecture.png" alt="MarineGEO circle logo" style="width: 90%; height: 70%;"/>

### Deploying application | AWS - EC2

##### Steps

1. Create instance EC2 - AWS Linux (recommendable launch with __t2.large__)
2. Configuration Inbound (Security)
3. [Important] create variables for Gitlab CI/CD

        Settings > CI/CD > Variables
        a. SSH_PRIVATE_KEY (type file, private key - type .pem)
        b. EC2_USER (user of your instance)
        c. EC2_HOST (host or IP)

4. Install docker in **AWS - Linux**

   a. Install docker

           # Update the package list
           sudo yum update -y

           # Install Docker
           sudo amazon-linux-extras install docker -y

           # Start the Docker service
           sudo service docker start

           # Enable Docker to start on boot
           sudo systemctl enable docker

           # Add the current user to the docker group to run Docker without sudo
           sudo usermod -aG docker $USER

           # Log out and log back in to apply group changes or use the following command to refresh the groups
           newgrp docker

           # Verify Docker installation
           docker --version

   b. Install Docker compose

           # Download the latest version of Docker Compose
           sudo curl -L "https://github.com/docker/compose/releases/download/v2.21.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

           # Apply executable permissions to the Docker Compose binary
           sudo chmod +x /usr/local/bin/docker-compose

           # Verify the installation
           docker-compose --version
            
           # Install for docker compose v1
           sudo yum install glibc-devel -y

5. Finally, run deploy and yey!!