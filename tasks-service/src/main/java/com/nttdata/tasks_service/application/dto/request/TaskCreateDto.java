package com.nttdata.tasks_service.application.dto.request;

import com.nttdata.tasks_service.domain.models.Task;
import com.nttdata.tasks_service.utils.StringGenerator;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public record TaskCreateDto(
	@Size(min = 4, max = 200)
	String title,

	@NotNull
	Long employeeId
) {
	public Task toModel() {
		return Task.builder()
			.title(title)
			.slug(StringGenerator.generateSlug(title))
			.completed(false)
			.build();
	}
}
