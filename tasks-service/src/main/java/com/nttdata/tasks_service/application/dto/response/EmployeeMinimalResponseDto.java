package com.nttdata.tasks_service.application.dto.response;

import com.nttdata.tasks_service.domain.models.Employee;

public record EmployeeMinimalResponseDto(
	long id,
	String fullName
) {
	public static EmployeeMinimalResponseDto toJson(Employee employee) {
		return new EmployeeMinimalResponseDto(
			employee.getId(),
			employee.getFullName()
		);
	}
}
