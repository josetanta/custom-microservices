package com.nttdata.tasks_service.application.dto.response;

import com.nttdata.tasks_service.domain.models.Task;

import java.io.Serializable;

public record TaskResponseDto(
	long id,
	String title,
	String slug,
	boolean completed,
	EmployeeMinimalResponseDto owner
) implements Serializable {
	public static TaskResponseDto toJson(Task task) {
		return new TaskResponseDto(
			task.getId(),
			task.getTitle(),
			task.getSlug(),
			task.isCompleted(),
			new EmployeeMinimalResponseDto(
				task.getOwner().getId(),
				task.getOwner().getFullName()
			)
		);
	}
}
