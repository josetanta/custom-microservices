package com.nttdata.tasks_service.application.rest;

import com.nttdata.tasks_service.application.dto.request.EmployeeCreatedDto;
import com.nttdata.tasks_service.application.dto.response.EmployeeResponseDto;
import com.nttdata.tasks_service.domain.services.EmployeeService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/employees")
public class EmployeeAPIRest {

	private final EmployeeService employeeService;

	@PostMapping
	public ResponseEntity<EmployeeResponseDto> postRegisterEmployee(
		@Valid @RequestBody EmployeeCreatedDto dto
	) {
		var resp = EmployeeResponseDto.toJson(employeeService.create(dto.toModel()));
		return ResponseEntity.created(URI.create("/api/employees")).body(resp);
	}

	@GetMapping
	public ResponseEntity<List<EmployeeResponseDto>> getListEmployee() {
		return ResponseEntity.ok(employeeService.listObject()
			.stream()
			.map(EmployeeResponseDto::toJson)
			.toList()
		);
	}

	@GetMapping("{id}")
	public ResponseEntity<?> getSingleEmployee(@PathVariable Long id) {
		return ResponseEntity.ok(employeeService.singleObject(id));
	}
}
