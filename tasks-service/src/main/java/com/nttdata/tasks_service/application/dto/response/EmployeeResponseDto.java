package com.nttdata.tasks_service.application.dto.response;

import com.nttdata.tasks_service.domain.models.Employee;

public record EmployeeResponseDto(
	long id,
	String fullName,
	boolean active
) {

	public static EmployeeResponseDto toJson(Employee employee) {
		return new EmployeeResponseDto(
			employee.getId(),
			employee.getFullName(),
			employee.isActive()
		);
	}
}
