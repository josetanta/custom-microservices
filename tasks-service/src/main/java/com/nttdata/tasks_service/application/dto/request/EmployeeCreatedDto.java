package com.nttdata.tasks_service.application.dto.request;

import com.nttdata.tasks_service.domain.models.Employee;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

public record EmployeeCreatedDto(
	@NotBlank
	@NotEmpty
	@Size(min = 4, max = 50)
	String fullName
) {
	public Employee toModel() {
		return Employee.builder()
			.fullName(fullName)
			.build();
	}
}
