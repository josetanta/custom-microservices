package com.nttdata.tasks_service.application.rest;

import com.nttdata.tasks_service.application.dto.request.TaskCreateDto;
import com.nttdata.tasks_service.application.dto.response.TaskResponseDto;
import com.nttdata.tasks_service.domain.services.TaskService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.concurrent.ExecutionException;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/tasks")
public class TaskAPIRest {

	private final TaskService taskService;

	@PostMapping
	public ResponseEntity<TaskResponseDto> postCreateTask(
		@Valid @RequestBody TaskCreateDto dto
	) throws ExecutionException, InterruptedException {
		var newTask = TaskResponseDto.toJson(taskService.createTask(dto));
		return ResponseEntity.created(URI.create("/api/tasks")).body(newTask);
	}

	@GetMapping
	public ResponseEntity<Page<TaskResponseDto>> getAllTask(
		@RequestParam(defaultValue = "0") int page,
		@RequestParam(defaultValue = "10") int size
	) {
		var tasks = taskService.listTask(page, size)
			.stream()
			.map(TaskResponseDto::toJson)
			.toList();

		var pageTasks = new PageImpl<>(tasks);
		return ResponseEntity.ok(pageTasks);
	}

	@GetMapping("/page")
	public ResponseEntity<?> getAllTaskPage(
		@RequestParam(defaultValue = "0") int page,
		@RequestParam(defaultValue = "10") int size
	) {
		var tasks = taskService.listTask(PageRequest.of(page, size));
		return ResponseEntity.ok(tasks);
	}

	@DeleteMapping("{id}")
	public ResponseEntity<?> deleteTask(
		@PathVariable long id
	) {
		taskService.delete(id);
		return ResponseEntity.noContent().build();
	}
}
