package com.nttdata.tasks_service.application.rest.advice;

import edu.systemia.common.utils.ParseErrorsValidation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ValidationRestAdvice {
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<?> handleValidationException(
		MethodArgumentNotValidException ex
	) {
		var response = ParseErrorsValidation.parseErrors("validation_parameters", ex.getFieldErrors());
		return ResponseEntity.badRequest().body(response);
	}
}
