package com.nttdata.tasks_service.infrastructure.mappers.adapters;

import com.nttdata.tasks_service.domain.models.Employee;
import com.nttdata.tasks_service.infrastructure.mappers.EmployeeMapperPersistence;
import com.nttdata.tasks_service.infrastructure.persistence.entity.EmployeeEntity;
import org.springframework.stereotype.Component;


@Component
public class EmployeeMapperPersistenceAdapter implements EmployeeMapperPersistence {

	@Override
	public Employee toModel(EmployeeEntity employeeEntity) {
		return Employee.builder()
			.id(employeeEntity.getId())
			.fullName(employeeEntity.getFullName())
			.active(employeeEntity.getActive())
			.build();
	}

	@Override
	public EmployeeEntity toEntity(Employee employee) {
		return EmployeeEntity.builder()
			.id(employee.getId())
			.fullName(employee.getFullName())
			.active(employee.isActive())
			.build();
	}
}
