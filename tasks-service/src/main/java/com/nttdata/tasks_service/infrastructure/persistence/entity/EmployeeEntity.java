package com.nttdata.tasks_service.infrastructure.persistence.entity;


import edu.systemia.common.entity.AuditingBaseEntity;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "employees")
public class EmployeeEntity extends AuditingBaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "full_name")
	private String fullName;

	@Column(columnDefinition = "boolean default true")
	@Builder.Default
	private Boolean active = true;

	@OneToMany(mappedBy = "owner")
	private List<TaskEntity> tasks;
}
