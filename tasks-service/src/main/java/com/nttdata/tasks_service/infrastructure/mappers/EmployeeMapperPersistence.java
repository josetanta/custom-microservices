package com.nttdata.tasks_service.infrastructure.mappers;

import com.nttdata.tasks_service.domain.models.Employee;
import com.nttdata.tasks_service.infrastructure.persistence.entity.EmployeeEntity;

public interface EmployeeMapperPersistence extends MapperPersistence<Employee, EmployeeEntity> {
}
