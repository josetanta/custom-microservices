package com.nttdata.tasks_service.infrastructure.mappers;

import com.nttdata.tasks_service.domain.models.Task;
import com.nttdata.tasks_service.infrastructure.persistence.entity.TaskEntity;

public interface TaskMapperPersistence extends MapperPersistence<Task, TaskEntity> {
}
