package com.nttdata.tasks_service.infrastructure.persistence.repository;

import com.nttdata.tasks_service.domain.models.Task;
import com.nttdata.tasks_service.infrastructure.persistence.entity.TaskEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TaskRepository extends JpaRepository<TaskEntity, Long> {

	@Query("""
		select new com.nttdata.tasks_service.domain.models.Task(
			t.id,
			t.title,
			t.slug,
			t.completed
		)
		from TaskEntity t
		""")
	Page<Task> findAllTasks(Pageable pageable);

}
