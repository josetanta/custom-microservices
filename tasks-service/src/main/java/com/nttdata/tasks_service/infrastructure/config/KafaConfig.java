package com.nttdata.tasks_service.infrastructure.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
@EnableKafka
public class KafaConfig {

	@Bean
	public NewTopic tasksTopic() {
		return TopicBuilder
			.name("tasks")
			.replicas(2)
			.compact()
			.build();
	}

}
