package com.nttdata.tasks_service.infrastructure.persistence.entity;

import edu.systemia.common.entity.AuditingBaseEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "tasks", indexes = {
	@Index(columnList = "slug", name = "idx_tasks_slug", unique = true),
	@Index(columnList = "title", name = "idx_tasks_title", unique = true)
})
public class TaskEntity extends AuditingBaseEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(nullable = false)
	private String title;
	private String slug;
	private boolean completed;

	@ManyToOne
	@JoinColumn(name = "owner_id")
	private EmployeeEntity owner;
}
