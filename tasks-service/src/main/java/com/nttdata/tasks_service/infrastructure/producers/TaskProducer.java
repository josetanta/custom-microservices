package com.nttdata.tasks_service.infrastructure.producers;

import com.nttdata.tasks_service.domain.models.Task;
import edu.systemia.common.messages.TaskEventMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;

@Service
@Slf4j
@RequiredArgsConstructor
public class TaskProducer {
	private final KafkaTemplate<String, Object> kafkaTemplate;

	public void sendMessage(Task task) throws ExecutionException, InterruptedException {
		log.info("sending message {}", task);

		TaskEventMessage data = TaskEventMessage.builder()
			.completed(task.isCompleted())
			.id(task.getId())
			.slug(task.getSlug())
			.title(task.getTitle())
			.ownerId(task.getOwner().getId())
			.build();

		Message<TaskEventMessage> message = MessageBuilder.withPayload(data)
			.setHeader(KafkaHeaders.TOPIC, "tasks")
			.setHeader(KafkaHeaders.GROUP_ID, "group-microservices")
			.setHeader(KafkaHeaders.KEY, String.valueOf(data.getId()))
			.build();

		String key = kafkaTemplate.send(message)
			.get().getProducerRecord().key();
		log.info("producer with key {}", key);
	}
}
