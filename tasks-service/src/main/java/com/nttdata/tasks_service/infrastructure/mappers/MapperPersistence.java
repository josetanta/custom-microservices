package com.nttdata.tasks_service.infrastructure.mappers;

/**
 * @param <M> Model
 * @param <E> Entity (persistence)
 */
public interface MapperPersistence<M, E> {
	M toModel(E e);

	E toEntity(M m);
}
