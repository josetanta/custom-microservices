package com.nttdata.tasks_service.infrastructure.mappers.adapters;

import com.nttdata.tasks_service.domain.models.Task;
import com.nttdata.tasks_service.infrastructure.mappers.TaskMapperPersistence;
import com.nttdata.tasks_service.infrastructure.persistence.entity.TaskEntity;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Qualifier("taskMapper")
@Component
public class TaskMapperPersistenceAdapter implements TaskMapperPersistence {

	@Override
	public Task toModel(TaskEntity taskEntity) {
		return Task.builder()
			.id(taskEntity.getId())
			.completed(taskEntity.isCompleted())
			.title(taskEntity.getTitle())
			.slug(taskEntity.getSlug())
			.build();
	}

	@Override
	public TaskEntity toEntity(Task task) {
		return TaskEntity.builder()
			.id(task.getId())
			.title(task.getTitle())
			.completed(task.isCompleted())
			.slug(task.getSlug())
			.build();
	}
}
