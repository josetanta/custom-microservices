package com.nttdata.tasks_service.utils;

import java.util.Random;

public interface StringGenerator {

	static String generateSlug(String lineWord) {
		int leftLimit = 97; // letter 'a'
		int rightLimit = 122; // letter 'z'
		int targetStringLength = 10;
		Random random = new Random();

		return random.ints(leftLimit, rightLimit + 1)
			.limit(targetStringLength)
			.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
			.toString() + lineWord.strip().replaceAll(" ", "-");
	}
}
