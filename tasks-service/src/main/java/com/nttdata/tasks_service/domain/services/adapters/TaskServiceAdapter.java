package com.nttdata.tasks_service.domain.services.adapters;

import com.nttdata.tasks_service.application.dto.request.TaskCreateDto;
import com.nttdata.tasks_service.domain.models.Task;
import com.nttdata.tasks_service.domain.services.EmployeeService;
import com.nttdata.tasks_service.domain.services.TaskService;
import com.nttdata.tasks_service.infrastructure.mappers.EmployeeMapperPersistence;
import com.nttdata.tasks_service.infrastructure.mappers.TaskMapperPersistence;
import com.nttdata.tasks_service.infrastructure.persistence.repository.TaskRepository;
import com.nttdata.tasks_service.infrastructure.producers.TaskProducer;
import edu.systemia.common.exceptions.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

@Service
@RequiredArgsConstructor
public class TaskServiceAdapter implements TaskService {

	// Dependency injection
	private final TaskRepository taskRepository;
	private final TaskMapperPersistence taskMapper;

	private final EmployeeService employeeService;
	private final EmployeeMapperPersistence userMapper;

	private final TaskProducer taskProducer;

	@Override
	public Task createTask(TaskCreateDto dto) throws ExecutionException, InterruptedException {
		var owner = Optional.ofNullable(employeeService.singleObject(dto.employeeId()))
			.orElseThrow(() -> new NotFoundException("Employee not found with id " + dto.employeeId()));

		var entity = taskMapper.toEntity(dto.toModel());
		entity.setOwner(userMapper.toEntity(owner));

		var savedTask = taskMapper.toModel(taskRepository.save(entity));
		savedTask.setOwner(owner);

		// Emit producer
		taskProducer.sendMessage(savedTask);

		return savedTask;
	}

	@Override
	public List<Task> listTask(int... pages) {
		return taskRepository.findAll(PageRequest.of(pages[0], pages[1]))
			.stream()
			.map(t -> {
				var model = taskMapper.toModel(t);
				model.setOwner(userMapper.toModel(t.getOwner()));
				return model;
			})
			.toList();
	}

	@Override
	public Page<Task> listTask(Pageable pageable) {
		return taskRepository.findAllTasks(pageable);
	}

	@Override
	public void update(Task task) {

	}

	@Override
	public void delete(Long id) {
		taskRepository.delete(taskMapper.toEntity(singleObject(id)));
	}

	@Override
	public List<Task> listObject() {
		return listTask(0, (int) taskRepository.count());
	}

	@Override
	public Task singleObject(Long id) {
		return taskRepository.findById(id)
			.map(taskMapper::toModel)
			.orElseThrow();
	}
}
