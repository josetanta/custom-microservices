package com.nttdata.tasks_service.domain.services;

import java.util.List;

public interface CrudService<T, ID> {
	default T create(T t) {
		return null;
	}

	void update(T t);

	void delete(ID id);

	List<T> listObject();

	T singleObject(ID id);
}
