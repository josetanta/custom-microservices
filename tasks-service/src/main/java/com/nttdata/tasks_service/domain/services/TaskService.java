package com.nttdata.tasks_service.domain.services;

import com.nttdata.tasks_service.application.dto.request.TaskCreateDto;
import com.nttdata.tasks_service.domain.models.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.concurrent.ExecutionException;

public interface TaskService extends CrudService<Task, Long> {
	// Use cases
	Task createTask(TaskCreateDto dto) throws ExecutionException, InterruptedException;

	List<Task> listTask(int... pages);

	Page<Task> listTask(Pageable pageable);
}
