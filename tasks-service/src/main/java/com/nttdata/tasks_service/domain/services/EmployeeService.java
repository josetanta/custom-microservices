package com.nttdata.tasks_service.domain.services;

import com.nttdata.tasks_service.domain.models.Employee;

public interface EmployeeService extends CrudService<Employee, Long> {
}
