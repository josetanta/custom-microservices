package com.nttdata.tasks_service.domain.services.adapters;

import com.nttdata.tasks_service.domain.models.Employee;
import com.nttdata.tasks_service.domain.services.EmployeeService;
import com.nttdata.tasks_service.infrastructure.mappers.EmployeeMapperPersistence;
import com.nttdata.tasks_service.infrastructure.persistence.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeServiceAdapter implements EmployeeService {

	private final EmployeeRepository employeeRepository;
	private final EmployeeMapperPersistence employeeMapper;

	@Transactional
	@Override
	public Employee create(Employee employee) {
		employee.setTasks(List.of());
		var employeeSaved = employeeRepository.save(employeeMapper.toEntity(employee));
		return employeeMapper.toModel(employeeSaved);
	}

	@Override
	public void update(Employee employee) {
		employeeRepository.save(employeeMapper.toEntity(employee));
	}

	@Override
	public void delete(Long id) {
		employeeRepository.deleteById(id);
	}

	@Override
	public List<Employee> listObject() {
		return employeeRepository.findAll()
			.stream().map(employeeMapper::toModel)
			.toList();
	}

	@Override
	public Employee singleObject(Long id) {
		return employeeRepository.findEmployeeEntityById(id)
			.map(employeeMapper::toModel)
			//			.orElseThrow(() -> new NotFoundException("Employee with id " + id + " not found"));
			.orElse(null);
	}
}
