package com.nttdata.tasks_service.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Employee {
	private long id;
	private String fullName;
	@Builder.Default
	private boolean active = true;
	private List<Task> tasks;

	public Employee(long id, String fullName) {
		this.id = id;
		this.fullName = fullName;
	}

	public Employee(long id, String fullName, boolean active) {
		this.id = id;
		this.fullName = fullName;
		this.active = active;
	}
}
