package com.nttdata.tasks_service.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Task {
	private long id;
	private String title;
	private String slug;
	private boolean completed;
	private Employee owner;

	public Task(long id, String title, String slug, boolean completed) {
		this.id = id;
		this.title = title;
		this.slug = slug;
		this.completed = completed;
	}

}
