package com.nttdata.tasks_service.api_rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nttdata.tasks_service.application.dto.request.TaskCreateDto;
import com.nttdata.tasks_service.application.rest.TaskAPIRest;
import com.nttdata.tasks_service.domain.models.Employee;
import com.nttdata.tasks_service.domain.models.Task;
import com.nttdata.tasks_service.domain.services.EmployeeService;
import com.nttdata.tasks_service.domain.services.TaskService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(value = TaskAPIRest.class)
public class TaskAPIRestTest {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper mapper;

	@MockBean
	private TaskService taskService;

	@MockBean
	private EmployeeService employeeService;

	private Employee owner;

	@BeforeEach
	void beforeEachSetup() {
		owner = Employee.builder()
			.id(1)
			.fullName("usuario-nombre-completo-1")
			.active(true)
			.build();
		when(employeeService.create(any(Employee.class)))
			.thenReturn(owner);
	}

	@Test
	void givenTask_whenRequestToEndpoint_thenAssertionsResponseCreated() throws Exception {
		// Create a new task with
		var taskDto = new TaskCreateDto(
			"Task 1",
			owner.getId()
		);

		var taskSaved = Task.builder()
			.id(1)
			.title(taskDto.title())
			.owner(owner)
			.slug("slug-tasks")
			.completed(false)
			.build();

		when(taskService.createTask(any(TaskCreateDto.class)))
			.thenReturn(taskSaved);

		mvc.perform(
				post("/api/tasks")
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON)
					.content(mapper.writeValueAsBytes(taskDto))
			)
			.andExpect(status().isCreated())
			.andExpect(jsonPath("$.title").value("Task 1"))
			.andDo(print());

		assertThat(taskSaved.getOwner()).isEqualTo(owner);
	}

	@Test
	void whenSendTaskWithBadTitle_thenResponseBadRequest() throws Exception {
		// Create a new task with
		var taskDto = new TaskCreateDto(
			"T-", // <----- this is an error
			owner.getId()
		);

		mvc.perform(
				post("/api/tasks")
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON)
					.content(mapper.writeValueAsBytes(taskDto))
			)
			.andExpect(status().isBadRequest())
			.andDo(print());
	}

	@Test
	void whenSendTaskWithoutOwnerId_thenResponseBadRequest() throws Exception {
		// Create a new task with
		var taskDto = new TaskCreateDto(
			"Task-01", // <----- this is an error
			null
		);

		mvc.perform(
				post("/api/tasks")
					.content(mapper.writeValueAsString(taskDto).getBytes(StandardCharsets.UTF_8))
					.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON)
			)
			.andExpect(status().isBadRequest())
			.andDo(print());
	}
}
