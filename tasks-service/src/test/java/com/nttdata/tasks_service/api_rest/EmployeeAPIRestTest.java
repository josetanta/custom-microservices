package com.nttdata.tasks_service.api_rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nttdata.tasks_service.application.dto.request.EmployeeCreatedDto;
import com.nttdata.tasks_service.application.rest.EmployeeAPIRest;
import com.nttdata.tasks_service.domain.models.Employee;
import com.nttdata.tasks_service.domain.services.EmployeeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = EmployeeAPIRest.class)
public class EmployeeAPIRestTest {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper mapper;

	@MockBean
	private EmployeeService employeeService;

	@Test
	void givenUser_thenResponseWillBeCreated() throws Exception {
		var user = Employee.builder()
			.id(1)
			.active(true)
			.fullName("usuario-nombre-completo-1")
			.tasks(List.of())
			.build();

		when(employeeService.create(any(Employee.class)))
			.thenReturn(user);

		var userDto = new EmployeeCreatedDto("usuario-nombre-completo-1");

		mvc.perform(
				post("/api/employees")
					.content(mapper.writeValueAsBytes(userDto))
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
			)
			.andExpect(status().isCreated())
			.andExpect(jsonPath("$").isNotEmpty())
			.andExpect(jsonPath("$.fullName").value("usuario-nombre-completo-1"))
			.andExpect(jsonPath("$.active").isBoolean())
			.andDo(print());
	}

	@Test
	void givenUserWithUsernameIncorrect_thenResponseWillBeBadRequest() throws Exception {
		// Given a user
		var userDto = new EmployeeCreatedDto("u");

		// When
		mvc.perform(
				post("/api/employees")
					.content(mapper.writeValueAsBytes(userDto))
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
			)

			// Then
			.andExpect(status().isBadRequest())
			.andDo(print());
	}
}
