package com.nttdata.tasks_service.repository;

import com.nttdata.tasks_service.infrastructure.persistence.entity.EmployeeEntity;
import com.nttdata.tasks_service.infrastructure.persistence.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class EmployeeRepositoryTest {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Test
	void createUserCorrectly() {
		var user = EmployeeEntity.builder()
			.fullName("user-name-01")
			.build();

		var userSaved = employeeRepository.save(user);
		assertNotNull(userSaved);
		assertEquals(1, employeeRepository.count());
	}

	@Test
	void whenCreateUser_thenAssertionsActiveEqualTrue() {
		var u1 = EmployeeEntity.builder()
			.fullName("user-name-01")
			.build();

		var userSaved = employeeRepository.save(u1);

		assertEquals(1, employeeRepository.count());
		assertTrue(userSaved.getActive());
	}

	//	@Test
	//	void whenCreateTwoUsersWithSameUsername_thenAssertionsWithError() {
	//		var u1 = EmployeeEntity.builder()
	//			.fullName("user-name-01")
	//			.build();
	//
	//		var u2 = EmployeeEntity.builder()
	//			.fullName("user-name-01")
	//			.build();
	//
	//		assertThrows(DataIntegrityViolationException.class, () -> {
	//			employeeRepository.saveAll(List.of(u1, u2));
	//			assertEquals(0, employeeRepository.count());
	//		});
	//	}
}
