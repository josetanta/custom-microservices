package com.nttdata.tasks_service.repository;

import com.nttdata.tasks_service.infrastructure.persistence.entity.EmployeeEntity;
import com.nttdata.tasks_service.infrastructure.persistence.entity.TaskEntity;
import com.nttdata.tasks_service.infrastructure.persistence.repository.EmployeeRepository;
import com.nttdata.tasks_service.infrastructure.persistence.repository.TaskRepository;
import com.nttdata.tasks_service.utils.StringGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.DirtiesContext;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class TaskRepositoryTest {

	@Autowired
	private TaskRepository taskRepository;

	@Autowired
	private EmployeeRepository employeeRepository;

	private EmployeeEntity owner;

	@BeforeEach
	void beforeEachSetup() {
		owner = employeeRepository.save(EmployeeEntity.builder()
			.fullName("user-1")
			.active(false)
			.build());
	}

	@Test
	void createATaskCorrectly() {
		var taskEntity = TaskEntity.builder()
			.title("Task 1")
			.completed(false)
			.slug(StringGenerator.generateSlug("task-1"))
			.owner(owner)
			.createdDate(LocalDateTime.now())
			.build();

		var taskSaved = taskRepository.save(taskEntity);

		assertThat(taskSaved).isNotNull();
		assertThat(taskRepository.count()).isEqualTo(1);
	}

	@Test
	void whenCreateATaskWithoutTitle_thenAssertionsErrorOfNullableValue() {
		var taskEntity = TaskEntity.builder()
			//			.title("Task 1")
			.completed(false)
			.slug(StringGenerator.generateSlug("task-1"))
			.owner(owner)
			.build();

		assertThrows(DataIntegrityViolationException.class, () -> {
				taskRepository.save(taskEntity);
				assertThat(taskRepository.count()).isEqualTo(0);
			}
		);
	}

	@Test
	void givenTwoTasksWithTheSameTitle_whenCreateThese_thenAssertionsConstraints() {
		var t1 = TaskEntity.builder()
			.title("Task-with-same-title")
			.completed(false)
			.slug("task-1")
			.owner(owner)
			.build();

		var t2 = TaskEntity.builder()
			.title("Task-with-same-title")
			.completed(false)
			.slug("task-2")
			.owner(owner)
			.build();

		assertThrowsExactly(DataIntegrityViolationException.class, () -> {
				taskRepository.saveAll(
					List.of(t1, t2)
				);
				assertThat(taskRepository.count()).isEqualTo(0);
			}
		);
	}

	@Test
	void givenTwoTasksWithTheSameSlug_whenCreateThese_thenAssertionsConstraints() {
		var t1 = TaskEntity.builder()
			.title("Task 1")
			.completed(false)
			.slug("task-1")
			.owner(owner)
			.build();

		var t2 = TaskEntity.builder()
			.title("Task 2")
			.completed(false)
			.slug("task-1")
			.owner(owner)
			.build();

		assertThrowsExactly(DataIntegrityViolationException.class, () -> {
				taskRepository.saveAll(
					List.of(t1, t2)
				);
				assertThat(taskRepository.count()).isEqualTo(0);
			}
		);
	}
}
