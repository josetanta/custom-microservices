package edu.system.documents_service.infrastructure.client;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component("storageThings")
@Getter
@Setter
public class StorageThings {
	private String authorization;
}
