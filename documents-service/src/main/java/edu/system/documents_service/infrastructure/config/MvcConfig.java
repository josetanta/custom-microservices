package edu.system.documents_service.infrastructure.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class MvcConfig {

	@Bean
	public RestTemplate restTemplate() {
		var template = new RestTemplate();
		List<ClientHttpRequestInterceptor> interceptors = template.getInterceptors();
		if (CollectionUtils.isEmpty(interceptors))
			interceptors = new ArrayList<>();

		interceptors.add(new RestTemplateBodyModifierInterceptor());
		template.setInterceptors(interceptors);
		return template;
	}
}
