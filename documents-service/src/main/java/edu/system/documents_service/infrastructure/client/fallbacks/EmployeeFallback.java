package edu.system.documents_service.infrastructure.client.fallbacks;

import edu.system.documents_service.application.dto.EmployeeDTO;

import java.util.Optional;

public class EmployeeFallback {

	public Optional<EmployeeDTO> findById(Long id) {
		return Optional.empty();
	}
}
