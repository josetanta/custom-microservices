package edu.system.documents_service.infrastructure.consumers;

import com.fasterxml.jackson.core.JsonProcessingException;
import edu.system.documents_service.application.dto.DocumentCreateDTO;
import edu.system.documents_service.domain.services.DocumentService;
import edu.systemia.common.messages.TaskEventMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class TaskConsumer {

	private final DocumentService documentService;

	@KafkaListener(groupId = "group-microservices", topics = "tasks")
	public void listen(@Payload ConsumerRecord<String, TaskEventMessage> data) throws JsonProcessingException {
		log.info("Payload {}", data);
		// If the task is completed, then create a document
		// with the title
		var value = data.value();
		var documentNew = new DocumentCreateDTO(value.getTitle(), value.getOwnerId());
		documentService.createDocument(documentNew);
		log.info("Create a new document");
	}
}
