package edu.system.documents_service.infrastructure.client;

import edu.system.documents_service.application.dto.EmployeeDTO;
import edu.system.documents_service.infrastructure.client.fallbacks.EmployeeFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(
	url = "${tasks-service-host}",
	name = "employeeFeignService",
	fallback = EmployeeFallback.class
)
public interface EmployeeFeignClient {

	@GetMapping("/tasks-service/employees/{id}")
	Optional<EmployeeDTO> findById(@PathVariable Long id);
}
