package edu.system.documents_service.infrastructure.client;

import edu.system.documents_service.application.dto.TaskListDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(
	url = "${tasks-service-host}",
	name = "tasksFeignService"
)
public interface TaskFeignClient {

	@GetMapping(value = "/tasks-service/tasks")
	TaskListDTO getListTask();
}
