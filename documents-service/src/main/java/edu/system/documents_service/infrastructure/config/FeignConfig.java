package edu.system.documents_service.infrastructure.config;

import edu.system.documents_service.infrastructure.client.StorageThings;
import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfig {

	@Autowired
	private StorageThings storageThings;

	@Bean
	public RequestInterceptor requestInterceptor() {
		return request -> request.header("Authorization", storageThings.getAuthorization());
	}

}
