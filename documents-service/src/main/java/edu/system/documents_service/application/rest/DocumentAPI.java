package edu.system.documents_service.application.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import edu.system.documents_service.application.dto.DocumentCreateDTO;
import edu.system.documents_service.domain.models.Document;
import edu.system.documents_service.domain.services.DocumentService;
import edu.system.documents_service.infrastructure.client.StorageThings;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/documents")
@RequiredArgsConstructor
public class DocumentAPI {

	private final DocumentService documentServiceAdapter;
	private final StorageThings storageThings;

	@GetMapping(params = {"id"})
	@RateLimiter(name = "default")
	public ResponseEntity<Document> getSingleDocument(@RequestParam("id") Long documentId) {
		return ResponseEntity.ok(documentServiceAdapter.getSingleDoc(documentId));
	}

	@GetMapping
	@RateLimiter(name = "pagination")
	public ResponseEntity<?> getAllDocuments(@RequestParam(name = "page", defaultValue = "0") int page, @RequestParam(name = "size", defaultValue = "10") int size) {
		Pageable pageable = PageRequest.of(page, size);
		return ResponseEntity.ok(documentServiceAdapter.getAllDocs(pageable));
	}

	@PostMapping
	@RateLimiter(name = "default")
	public ResponseEntity<Document> postNewDocument(@Valid @RequestBody DocumentCreateDTO documentCreateDTO, @RequestHeader HttpHeaders headers) throws JsonProcessingException {
		var authorization = headers.getOrEmpty("authorization").get(0);
		storageThings.setAuthorization(authorization);
		return ResponseEntity.ok(documentServiceAdapter.createSingleDoc(documentCreateDTO));
	}
}
