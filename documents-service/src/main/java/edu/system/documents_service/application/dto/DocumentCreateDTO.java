package edu.system.documents_service.application.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record DocumentCreateDTO(
	@NotNull
	@NotBlank
	String nameDoc,

	//	@Null.List(value = {})
	//	List<String> words

	@NotNull
	@Min(1)
	Long employeeId
) {
}
