package edu.system.documents_service.application.dto;

public record EmployeeDTO(
	long id,
	String fullName,
	boolean active
) {
}
