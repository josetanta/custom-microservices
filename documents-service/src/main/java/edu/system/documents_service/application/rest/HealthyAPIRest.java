package edu.system.documents_service.application.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/healthy")
public class HealthyAPIRest {

	@GetMapping
	public ResponseEntity<String> getHealthy() {
		return ResponseEntity.ok("Ok");
	}
}
