package edu.system.documents_service.application.rest.advices;

import edu.systemia.common.exceptions.HandleException;
import edu.systemia.common.exceptions.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class AppExceptionHandler extends ResponseEntityExceptionHandler {

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler({NotFoundException.class})
	public Map<String, Object> handleNotFoundEntity(HandleException ex) {

		Map<String, Object> attrs = new HashMap<>();
		attrs.put("message", ex.getMessage());
		attrs.put("status", ex.getStatusCode());
		return attrs;
	}
}
