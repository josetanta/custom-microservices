package edu.system.documents_service.application.rest;

import edu.system.documents_service.domain.models.Task;
import edu.system.documents_service.infrastructure.client.StorageThings;
import edu.system.documents_service.infrastructure.client.TaskFeignClient;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/task")
@RequiredArgsConstructor
public class TaskAPI {

	private final TaskFeignClient taskFeign;
	private final StorageThings storageThings;

	@GetMapping("/list")
	public ResponseEntity<List<Task>> getAllTask(
		@RequestHeader HttpHeaders headers
	) {
		var authorization = headers.getOrEmpty("authorization").get(0);
		storageThings.setAuthorization(authorization);
		return ResponseEntity.ok(taskFeign.getListTask().content());
	}
}
