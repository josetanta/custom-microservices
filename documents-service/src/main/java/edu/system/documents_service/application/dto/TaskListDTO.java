package edu.system.documents_service.application.dto;

import edu.system.documents_service.domain.models.Task;

import java.util.List;

public record TaskListDTO(
	List<Task> content
) {
}
