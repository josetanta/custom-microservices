package edu.system.documents_service.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Task implements Serializable {
	private long id;
	private String title;
	private String slug;
	private List<String> subTasks;
	private boolean completed;
	private Map<String, Object> owner;
}
