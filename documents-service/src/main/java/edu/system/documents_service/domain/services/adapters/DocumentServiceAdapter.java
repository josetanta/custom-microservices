package edu.system.documents_service.domain.services.adapters;

import edu.system.documents_service.application.dto.DocumentCreateDTO;
import edu.system.documents_service.application.dto.EmployeeDTO;
import edu.system.documents_service.domain.models.Document;
import edu.system.documents_service.domain.services.DocumentService;
import edu.system.documents_service.infrastructure.client.EmployeeFeignClient;
import edu.system.documents_service.infrastructure.persistence.repository.DocumentRepository;
import edu.systemia.common.exceptions.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DocumentServiceAdapter implements DocumentService {

	private final DocumentRepository documentRepository;

	private final EmployeeFeignClient employeeFeignClient;

	@Override
	public Document getSingleDoc(Long documentId) {
		return documentRepository.findById(documentId).orElseThrow(() -> new IllegalArgumentException("Document not found with id " + documentId));
	}

	@Override
	public List<Document> getAllDocs() {
		return documentRepository.findAll();
	}

	@Override
	public Document createSingleDoc(DocumentCreateDTO documentCreateDTO) {

		EmployeeDTO employeeDTO = employeeFeignClient.findById(documentCreateDTO.employeeId())
			.orElseThrow(() -> new NotFoundException("Employee not found with id " + documentCreateDTO.employeeId()));

		Document documentNew = Document.builder()
			.employeeId(documentCreateDTO.employeeId())
			.nameDoc(documentCreateDTO.nameDoc())
			.build();

		return documentRepository.save(documentNew);
	}

	@Override
	public Page<Document> getAllDocs(Pageable pageable) {
		return documentRepository.findAll(pageable);
	}

	@Override
	public Document createDocument(DocumentCreateDTO documentCreateDTO) {
		Document documentNew = Document.builder()
			.employeeId(documentCreateDTO.employeeId())
			.nameDoc(documentCreateDTO.nameDoc())
			.build();

		return documentRepository.save(documentNew);
	}
}
