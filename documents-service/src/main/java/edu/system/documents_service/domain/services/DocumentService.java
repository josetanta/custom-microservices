package edu.system.documents_service.domain.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import edu.system.documents_service.application.dto.DocumentCreateDTO;
import edu.system.documents_service.domain.models.Document;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service interface for managing documents.
 * Provides methods for retrieving, creating, and listing documents.
 */
public interface DocumentService {

	/**
	 * Retrieves a single document by its unique identifier.
	 *
	 * @param documentId the unique identifier of the document to retrieve
	 * @return the document corresponding to the given ID
	 */
	Document getSingleDoc(Long documentId);

	/**
	 * Retrieves a list of all documents.
	 *
	 * @return a list of all available documents
	 */
	List<Document> getAllDocs();

	/**
	 * Creates a new document based on the provided data transfer object.
	 *
	 * @param documentCreateDTO the data transfer object containing the information for the new document
	 * @return the created document
	 * @throws JsonProcessingException if there is an error processing JSON data during document creation
	 */
	Document createSingleDoc(DocumentCreateDTO documentCreateDTO) throws JsonProcessingException;

	/**
	 * Retrieves a paginated list of all documents.
	 *
	 * @param pageable the pagination information
	 * @return a paginated list of documents
	 */
	Page<Document> getAllDocs(Pageable pageable);

	/**
	 * Creates a new document based on the provided data transfer object.
	 *
	 * @param documentCreateDTO the data transfer object containing the information for the new document
	 * @return the created document
	 */
	Document createDocument(DocumentCreateDTO documentCreateDTO);
}

