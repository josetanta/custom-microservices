package edu.system.documents_service.domain.models;

import edu.systemia.common.entity.AuditingBaseEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Data
@AllArgsConstructor
@NoArgsConstructor

@Entity
public class Document extends AuditingBaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "name_doc", length = 520, nullable = false)
	private String nameDoc;

	@Column(name = "employee_id", nullable = false)
	private Long employeeId;
}
