package org.cloudspring.gatewayservice.utils;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Predicate;

@Component
public class RouteValidator {

	public static final List<String> GUEST_ENDPOINTS = List.of(
		"/security/auth/register",
		"/security/auth/sign"
	);

	public Predicate<ServerHttpRequest> isSecured = request -> GUEST_ENDPOINTS.stream()
		.noneMatch(path -> request.getURI().getPath().contains(path));
}
