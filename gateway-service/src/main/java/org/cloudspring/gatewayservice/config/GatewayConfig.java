package org.cloudspring.gatewayservice.config;

import lombok.RequiredArgsConstructor;
import org.cloudspring.gatewayservice.filter.AuthenticationFilter;
import org.cloudspring.gatewayservice.utils.JwtUseCase;
import org.cloudspring.gatewayservice.utils.RouteValidator;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import reactor.core.publisher.Mono;

@Configuration
@RequiredArgsConstructor
public class GatewayConfig {

	private final JwtUseCase jwtUseCase;
	private final RouteValidator routeValidator;

	@Bean
	@Order(1)
	public GlobalFilter globalFilter() {
		return new AuthenticationFilter(jwtUseCase, routeValidator);
	}

	@Bean
	public KeyResolver userKeyResolver() {
		return exchange -> {
			String authHeader = exchange.getRequest().getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
			if (authHeader != null && authHeader.startsWith("Bearer ")) {
				return Mono.just(authHeader);
			}
			return Mono.empty();
		};
	}
}
