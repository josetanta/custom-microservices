package org.cloudspring.gatewayservice.filter;

import lombok.RequiredArgsConstructor;
import org.cloudspring.gatewayservice.utils.JwtUseCase;
import org.cloudspring.gatewayservice.utils.RouteValidator;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@RefreshScope
@RequiredArgsConstructor
public class AuthenticationFilter implements GlobalFilter, Ordered {

	private final JwtUseCase jwtUseCase;
	private final RouteValidator routeValidator;

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
		ServerHttpRequest request = exchange.getRequest();

		if (routeValidator.isSecured.test(request)) {
			if (isAuthMissing(request))
				return onError(exchange);

			final String token = extractAuthHeader(request);

			if (!jwtUseCase.isTokenValid(token))
					return onError(exchange);
		}
		return chain.filter(exchange);
	}

	private Mono<Void> onError(ServerWebExchange exchange) {
		ServerHttpResponse response = exchange.getResponse();
		response.setStatusCode(HttpStatus.UNAUTHORIZED);
		return response.setComplete();

	}

	private boolean isAuthMissing(ServerHttpRequest request) {
		return !request.getHeaders().containsKey(HttpHeaders.AUTHORIZATION);
	}

	private String extractAuthHeader(ServerHttpRequest request) {
		return request.getHeaders().getOrEmpty(HttpHeaders.AUTHORIZATION).get(0).substring(7);
	}

	@Override
	public int getOrder() {
		return -1;
	}
}
